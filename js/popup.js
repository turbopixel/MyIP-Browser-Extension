/**
 * highlight/select text
 * @param element
 */
function SelectText(element) {
  let doc = document,
    text = doc.getElementById(element),
    range, selection;

  if (doc.body.createTextRange) {
    range = document.body.createTextRange();
    range.moveToElementText(text);
    range.select();
  } else if (window.getSelection) {
    selection = window.getSelection();
    range = document.createRange();
    range.selectNodeContents(text);
    selection.removeAllRanges();
    selection.addRange(range);
  }
}

/**
 * fetch ip information
 */
async function fetchIp(host) {
  return await new Promise(async function (presp, preject) {
    let r = new XMLHttpRequest();
    r.open("GET", host, true);
    r.onreadystatechange = function () {
      if (r.readyState !== 4 || r.status !== 200) return;
      presp(r.responseText);
    };
    r.send();
  });
}

/**
 * select text handler
 * @param e Element
 */
document.onclick = function (e) {
  if (e.target.id === 'remoteIP') {
    SelectText('remoteIP');
  }
};

window.onload = async function () {
  let host_ipapi = "https://api.seeip.org/geoip";

  // fetch data
  let result_ip = await fetchIp(host_ipapi);

  if (result_ip) {
    document.getElementById("img-loading").style.display = 'none';
    document.getElementById("show-ip").style.display = 'inherit';
    document.getElementById("show-more").style.display = 'inherit';
  }

  // json parse
  let ip_object = JSON.parse(result_ip);

  // data object
  let information = new Object({
    remoteCountry: (typeof ip_object.country !== "undefined") ? ip_object.country : "unknown",
    regionName: (typeof ip_object.region !== "undefined") ? ip_object.region : "unknown",
    region: (typeof ip_object.region_code !== "undefined") ? ip_object.region_code : "unknown",
    countryCode: (typeof ip_object.country_code !== "undefined") ? ip_object.country_code : "unknown",
    remoteZip: (typeof ip_object.zip !== "undefined") ? ip_object.zip : "unknown",
    remoteCity: (typeof ip_object.city !== "undefined") ? ip_object.city : "unknown",
    timezone: (typeof ip_object.timezone !== "undefined") ? ip_object.timezone : "unknown",
    isp: (typeof ip_object.organization !== "undefined") ? ip_object.organization : "unknown",
    query: (typeof ip_object.ip !== "undefined") ? ip_object.ip : "unknown",
  });

  // add data into document
  document.getElementById("remoteCountry").innerText = information.remoteCountry + " (" + information.countryCode + ")";
  document.getElementById("remoteRegion").innerText = information.regionName + " (" + information.region + ")";
  document.getElementById("remoteTimezone").innerText = information.timezone;
  document.getElementById("remoteIsp").innerText = information.isp;
  document.getElementById("remoteIP").innerText = information.query;
};