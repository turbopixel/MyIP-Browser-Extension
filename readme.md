# MyIP - ip address and location details

This browser extension displays your public IPv4 address, provider name and some location details. Available for chromium based browsers and mozilla firefox.

## Browser variations

**Chromium**

> Microsoft Edge and Opera are based on Chromium and are compatible with the Google Chrome version.

* Install extension: [chrome.google.com/webstore/detail/myip-host-ip-isp-and-dns](https://chrome.google.com/webstore/detail/myip-host-ip-isp-and-dns/pdnildedfbigagjbaigbalnfdfpijhaf)
* [open branch: browser/chromium](https://codeberg.org/turbopixel/MyIP-Browser-Extension/src/branch/browser/chromium)


**Mozilla Firefox**

> The Firefox branch only contains a different manifest version.

* Install addon: [addons.mozilla.org/de/firefox/addon/myip](https://addons.mozilla.org/de/firefox/addon/myip/)
* [open branch: browser/firefox](https://codeberg.org/turbopixel/MyIP-Browser-Extension/src/branch/browser/firefox)

## Privacy

This extension uses the free API www.seeip.org. (It's open source: https://github.com/fcambus/telize)